package her.blok2.b_ict.hz.com.blok2her.model;

/**
 * Created by anton on 21-1-16.
 */
public class ChartPosition {
    private Chart chart;
    private int position;
    private MusicItem musicItem;

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public MusicItem getMusicItem() {
        return musicItem;
    }

    public void setMusicItem(MusicItem musicItem) {
        this.musicItem = musicItem;
    }

    public Chart getChart() {
        return chart;
    }

    public void setChart(Chart chart) {
        this.chart = chart;
    }
}
